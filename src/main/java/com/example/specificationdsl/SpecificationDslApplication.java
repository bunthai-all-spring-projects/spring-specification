package com.example.specificationdsl;

import com.example.specificationdsl.model.Customer;
import com.example.specificationdsl.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpecificationDslApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpecificationDslApplication.class, args);
	}

	@Autowired
	CustomerRepository customerRepository;

	@Override
	public void run(String... args) throws Exception {

		Customer customer1 = new Customer();
		customer1.setName("Bunthai");
		customer1.setSalary(2500.23);

		Customer customer2 = new Customer();
		customer2.setName("Deng");
		customer2.setSalary(550);

		Customer customer3 = new Customer();
		customer3.setName("Jing");
		customer3.setSalary(1000);


		customerRepository.save(customer1);
		customerRepository.save(customer2);
		customerRepository.save(customer3);
	}
}
