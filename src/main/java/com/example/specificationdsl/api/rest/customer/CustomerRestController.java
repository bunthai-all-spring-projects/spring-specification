package com.example.specificationdsl.api.rest.customer;

import com.example.specificationdsl.model.Customer;
import com.example.specificationdsl.repository.CustomerRepository;
import com.example.specificationdsl.specification.CustomerSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerRestController {

    @Autowired
    public CustomerRepository customerRepository;

    @GetMapping("test")
    public List<Customer> test() {

        return customerRepository.findAll(CustomerSpecification.topperSalary());

    }

}
