package com.example.specificationdsl.repository;

import com.example.specificationdsl.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor {
    List<Customer> findByName(String name);
}
